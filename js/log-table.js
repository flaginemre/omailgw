import settings from './settings.js';
import { updateLocale } from './i18n.js';

jQuery(function(){
    // Authentification par token
    let token = localStorage.getItem("token");
    // Détection Get param
    const locationHash = window.location.hash;
    const queryString = locationHash.split('?');
    debug("query : "+queryString[1]);
    const urlParams = new URLSearchParams(queryString[1]);
    var frmDateGet = urlParams.get('frmDate');
    var toDateGet = urlParams.get('toDate');
    var type = urlParams.get('type')
    if (frmDateGet == null && toDateGet == null) {
      debug("Default date");
      var frmDate = moment().subtract(settings.datatable_date_ago_default, "days").format("YYYY-MM-DD");
      var toDate = moment().format("YYYY-MM-DD");
      $("#toDate").val(toDate);
      $("#frmDate").val(frmDate);
    } else {
      var frmDate = frmDateGet;
      var toDate = toDateGet;
    }
    debug("type : "+type);
    if (type) {
      switch (type) {
        case "errorIn":
          // Changement du titre
          $('#log-table-title').html('Error input log table');
          // Affichage de certaine colonne ou non
          $('*[data-column="7"]').toggleClass('toggle-hide'); // smtpd_codeError
          $('*[data-column="9"]').toggleClass('toggle-hide'); // cleanup
          $('*[data-column="12"]').toggleClass('toggle-hide'); // qmgr_msg
          $('*[data-column="13"]').toggleClass('toggle-hide'); // msg
          break;
        case "errorOut":
          // Changement du titre
          $('#log-table-title').html('Error output log table');
          // Affichage de certaine colonne ou non
          $('*[data-column="18"]').toggleClass('toggle-hide'); // smtp_status
          $('*[data-column="19"]').toggleClass('toggle-hide'); // smtp_status
          break;
      }
    }
    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + token);
        }
    });
    // Setup - add a text input to each footer cell
    $('#dataTable tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" data-i18n-key="logSearch'+title+'" placeholder="Search by: ' + title + '" />');
    });

    var table = $('#dataTable').DataTable({
        order: [[2, 'desc']],
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: {
            "url": settings.api_url+settings.api_datatable,
            // On ajoute les dates de début et de fin
            "data": function (get) {
                get.frmDate = frmDate;
                get.toDate = toDate;
                get.type = type;
            },
        },
        columns: [
            { data: 'queueId' },
            { data: 'server' },
            { data: 'smtpd_date' },
            { data: 'from' },
            { data: 'smtp_to', name: 'log_smtps.smtp_to' },
            { data: 'smtpd_client' },
            { data: 'smtpd_username' },
            { data: 'smtpd_codeError' },
            { data: 'smtpd_msg' },
            { data: 'cleanup' },
            { data: 'qmgr_size' },
            { data: 'qmgr_nrcpt' },
            { data: 'qmgr_msg' },
            { data: 'msg' },
            { data: 'smtp_date', name: 'log_smtps.smtp_date' },
            { data: 'smtp_relay', name: 'log_smtps.smtp_relay' },
            { data: 'smtp_status', name: 'log_smtps.smtp_status' },
            { data: 'smtp_msg', name: 'log_smtps.smtp_msg' },

        ],
        //Pagination
        pagingType: 'full_numbers',
        // Ré-écriture de la date initialement en timestamp
        // https://datatables.net/plug-ins/dataRender/datetime
        columnDefs: [ {
            targets: 2,
            render: $.fn.dataTable.render.moment( 'X', 'lll' )
        } ],
        // Recherche par champs
        initComplete: function () {
          // Apply the search
          this.api()
              .columns()
              .every(function () {
                  var that = this;
                  $('input', this.footer()).on('keyup change clear', function (e) {
                      // Uniquement après avoir appuyé sur ENTER
                      if (e.keyCode == 13) {
                          if (that.search() !== this.value) {
                              that.search(this.value).draw();
                          }
                      }
                  });
              });
        },
        // Add class
        createdRow: function (row,data) {
          if (data.smtp_status != null && data.smtp_status != "sent") {
            $(row).addClass('table-danger');
          } else if (data.qmgr_msg != 'queue active' || data.smtpd_codeError != null || data.cleanup != null) {
            $(row).addClass('table-warning');
          }
        }
    });
    // Ne lancer la recherche qu'après avoir appuyer sur ENTER
    // https://datatables.net/forums/discussion/33028/searchdelay-for-server-side-issue
    $("div.dataTables_filter input").unbind();
    $("div.dataTables_filter input").keyup(function (e) {
        // Uniquement après avoir appuyé sur ENTER
        if (e.keyCode == 13) {
            table.search(this.value).draw();
        }
    });
    // Fonction de recherche par jour
    function searchByDate() {
        if (moment($("#frmDate").val()).format("X") > moment($("#toDate").val()).format("X")) {
            alert('Il y a une erreur dans la date');
        } else {
            frmDate = ($("#frmDate").val()) ? $("#frmDate").val() : '';
            toDate = ($("#toDate").val()) ? $("#toDate").val() : '';
            // Si c'est équivalent j'ajoute un jour au "to" ce qui permet d'afficher le contenu du jour
            if ($("#frmDate").val() != '' && $("#frmDate").val() == $("#toDate").val()) {
                toDate = moment($("#toDate").val()).add("days", 1).format('YYYY-MM-DD');
                $("#toDate").val(toDate);
            }
            table.draw();
        }
    }
    // On lance la recherche par date si des dates sont passés en paramètres
    if (frmDateGet != null && toDateGet != null) {
        $("#toDate").val(toDateGet);
        $("#frmDate").val(frmDateGet);
        searchByDate();
    }
    // Recherche par date si on clique sur le bouton
    $('#SearchByDate').on('click', function (e) {
        searchByDate();
    });



    // Afficher/masquer les colonnes au clic
    $('a.toggle-vis').on('click', function (e) {
        e.preventDefault();
        // Get the column API object
        var column = table.column($(this).attr('data-column'));
        $(this).toggleClass('toggle-hide');
        column.visible(!column.visible());
    });
    // On cache les colonnes par défaut
    $('.toggle-hide').each(function (i) {
        var column = table.column($(this).attr('data-column'));
        column.visible(!column.visible());
    });

    $('#dataTable').on('click', 'tbody tr', function () {
      var row = table.row($(this)).data();
      //debug(row);   //full row of array data
      //debug(row[1]);   //EmployeeId
      location.href='#one?queueId='+row.queueId;
    });

    $(document).ajaxStop(function () {
      $('#loadData').hide();
      //updateLocale();
    });
    $(document).ajaxStart(function () {
      $('#loadData').show();
    });
    updateLocale();
});