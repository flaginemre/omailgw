import { LogQueueId } from "./api_connector.js";
import { getSessionLocale } from './i18n.js';

// Update infos to show
jQuery(function () {
  const locationHash = window.location.hash;
  const queryString = locationHash.split('?');
  debug("query : " + queryString[1]);
  const urlParams = new URLSearchParams(queryString[1]);
  var queueIdGet = urlParams.get('queueId');
  showOne(queueIdGet);
});


async function showOne(queueId) {
  let locale = getSessionLocale();

  //récupérer les logs portant sur ce queueId;
  var logQueueIdTry = await LogQueueId(queueId);
  if (logQueueIdTry[0]) {
    var logs = logQueueIdTry[1];

    // feed 'from' origin card header
    $('#from-email').text(logs[0].from);

    // translate datetime
    let datetime = new Date(logs[0].smtpd_date * 1000),
      time = datetime.toLocaleTimeString(locale),
      date = datetime.toLocaleDateString(locale);
    // Feed smtpd origin card (use of ternary to hide null fields)
    let origin = `
      <div class="card-header">
        <div class="row">
          <div class="col">
            <i class="fas fa-server me-1"></i>
            <label class="text-muted" for="server" data-i18n-key="mailIncoming">Mail arrivant sur le serveur</label>
            <span class="p-2" name="server">${logs[0].server}</span>
          </div>
          <div class="col text-end">
            <label class="text-muted" for="smtpd_date" data-i18n-key="on">On</label>
            <span class="p-2" name="smtpd_date">${date}</span>
            <label class="text-muted" for="smtpd_hour" data-i18n-key="at">at</label>
            <span class="p-2" name="smtpd_hour">${time}</span>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class=col>
            ${logs[0].msg_id != null ? '<label class="text-muted" for="msg_id" data-i18n-key="mailnb"> mail n° </label>'+
            '<span class="p-2" name="msg_id">' + logs[0].msg_id + '</span>' : ''}
          </div>
        </div>
        <div class="row">
          <div class="col">
            <label class="text-muted" for="cleanup" data-i18n-key="cleanup"> cleanup </label>
            <span class="p-2" name="cleanup">${logs[0].cleanup != null ? logs[0].cleanup : '<span data-i18n-key="cleaned">cleaned</span>'}</span>
          </div>
        </div>
        <div class="row">
          <div class="col">
            ${logs[0].msg != null ? '<label class="text-muted" for="server" data-i18n-key="msg"> Message </label>'+
            '<span class="p-2" name="date">' + logs[0].msg + '</span>' : ''}
          </div>
        </div>
      `
    // Create div smtpd only if it holds any info
    if (logs[0].smtpd_client != null || logs[0].smtpd_username != null || logs[0].smtpd_codeError != null || logs[0].smtpd_msg != null) {
      origin += `
      <div class="card mb-3 mt-3">
        <div class="card-header">
          <i class="fas fa-arrow-right-to-bracket me-1"></i>
          <span data-i18n-key="smtpd">Daemon en entree</span>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col">
              ${logs[0].smtpd_client != null ? '<label class="text-muted" for="smtpd_client" data-i18n-key="smtpd_client"> Arrivé par le client </label>'+
              '<span class="p-2" name="smtpd_client">' + logs[0].smtpd_client + '</span>' : ''}
              ${logs[0].smtpd_username != null ? '<label class="text-muted" for="smtpd_username" data-i18n-key="smtpd_username"> Nom utilisateur </label>'+
              '<span class="p-2" name="smtpd_username">' + logs[0].smtpd_username + '</span>' : ''}
              ${logs[0].smtpd_codeError != null ? '<label class="text-muted" for="smtpd_codeError" data-i18n-key="smtpd_codeError" Code erreur </label>'+
              '<span class="p-2" name="smtpd_codeError">' + logs[0].smtpd_codeError + '</span>' : ''}
            </div>
          </div>
          <div class="row">
            <div class="col">
              ${logs[0].smtpd_msg != null ? '<label class="text-muted" for="smtpd_msg"> Message </label><span class="p-2" name="smtpd_msg">' + logs[0].smtpd_msg + '</span>' : ''}
            </div>
          </div>
        </div>
      </div>`
    }
    // create div qmgr only if holds any info
    if (logs[0].qmgr_size != null || logs[0].qmgr_nrcpt != null || logs[0].qmgr_msg != null) {
      let size = formatBytes(logs[0].qmgr_size);
      origin += `
      <div class="card mb-3 mt-3">
        <div class="card-header">
          <i class="fas fa-list-check me-1"></i>
          <span data-i18n-key="qmgr">Manageur file d'attente</span>
        </div>
        <div class="card-body">
          <div class="row">
            <div class=col>
              ${logs[0].qmgr_size != 0 ? '<label class="text-muted" for="qmgr_size" data-i18n-key="qmgr_size">Poids du mail</label><span class="p-2" name="qmgr_size">' + size + '</span>' : ''}
              ${logs[0].qmgr_nrcpt != null ? '<label class="text-muted" for="qmgr_nrcpt" data-i18n-key="qmgr_nrcpt">Destinataires</label><span class="p-2" name="qmgr_nrcpt">' + logs[0].qmgr_nrcpt + ' <span data-i18n-key="people">people</span></span>' : ''}
            </div>
          </div>
          <div class="row">
            <div class="col">
              ${logs[0].qmgr_msg != null ? '<label class="text-muted" for="qmgr_msg" data-i18n-key="qmgr_msg">Message</label><span class="p-2" name="qmgr_msg">' + logs[0].qmgr_msg + '</span>' : ''}
            </div>
          </div>
      </div>`
    }
    // end of origin feeding
    document.getElementById("origin").innerHTML = origin + `</div>`;


    // feed each 'to' card, and counting for the 'from' summary
    let nb_sent = 0,
      nb_deferred = 0,
      nb_blocked = 0,
      exp = document.getElementById("expedients");
    for (let i = 0; i < logs.length; i++) {
      let color = "",
        text = "";
      // translate 'to' datetime
      if (logs[i].smtp_date == null) {
        logs[i].smtp_date = logs[0].smtpd_date
      }
      let datetime_smtp = new Date(logs[i].smtp_date * 1000),
        time_smtp = datetime_smtp.toLocaleTimeString(locale),
        date_smtp = datetime_smtp.toLocaleDateString(locale);
      // ready up 'to' summary
      switch (logs[i].smtp_status) {
        case "sent":
          nb_sent++;
          color = "bg-success";
          text = "Parti";
          break;
        case "deferred":
          nb_deferred++;
          color = "bg-warning";
          text = "Reporté";
          break;
        default:
          nb_blocked++;
          color = "bg-danger";
          text = "Blocké";
          break;
      };
      let expedient =
        `
        <div class="card mx-4 mb-2" data-toggler="${i}">
          <div class="card-header">
            <div class="row">
              <div class="col">
                <h5>
                  <span class='toggleable'>-</span>  
                  ${logs[i].to}
                </h5>
              </div>
              <div class="col">
                <span class="px-2 py-1 rounded ${color} text-white">${text}</span>
              </div>
              <div class="col text-end">
                <label class="text-muted" for="smtp_date" data-i18n-key="on">On</label>
                <span class="p-2" name="smtp_date">${date_smtp}</span>
                <label class="text-muted" for="smtp_hour" data-i18n-key="at">at</label>
                <span class="p-2" name="smtp_hour">${time_smtp}</span>
              </div>
            </div>
          </div>`;
      // create div details only if holds any info
      if (logs[i].smtp_relay != null || logs[i].smtp_msg != null) {
        expedient += `
          <div class="card-body" data-toggled="${i}">
            <div class="row pl-4">
              <div class=col>
                ${logs[i].smtp_relay != null ? '<label class="text-muted" for="smtp_relay" data-i18n-key="smtp_relay" >Destination relay </label><span class="p-2" name="smtp_relay">' + logs[i].smtp_relay + '</span>' : ''}
              </div>
            </div>
            <div class="row">
              <div class="col">
                ${logs[i].smtp_msg != null ? '<label class="text-muted" for="smtp_msg" data-i18n-key="smtp_msg">Relay Message </label><span class="p-2" name="smtp_msg">' + logs[i].smtp_msg + '</span>' : ''}
              </div>
            </div>
          </div>
          `;
      }else{
        expedient += `
          <div class="card-body" data-toggled="${i}">
            <span data-i18n-key="noInfo">no info available</span>
          </div>`
      }
      exp.innerHTML += expedient + '</div>';
      if (logs[i].smtp_status == 'sent') {
        $('div[data-toggled=' + i + ']').hide();
        $('div[data-toggler=' + i + ']').find('.toggleable').text('+');
      }
    }
    //apply collapsible
    for (let i = 0; i < logs.length; i++) {
      let togglable = $('div[data-toggler=' + i + ']');
      togglable.hover(function () {
        $(this).css('cursor', 'pointer');
      });
      togglable.on('click', function () {
        let innerText = $('div[data-toggler=' + i + ']').find('.toggleable').text();
        $('div[data-toggler=' + i + ']').find('.toggleable').text(innerText == '+' ? '-' : '+');
        $('div[data-toggled=' + i + ']').toggle();
      });
    }
    // feed summary
    $('#queueId').text(logs[0].queueId);
    $('#nrcpt').text(logs[0].qmgr_nrcpt);

    if (nb_sent == 0) {
      $('#sent').toggleClass('bg-secondary bg-success');
    }
    $('#sent>[name=value]').text(nb_sent);

    if (nb_deferred == 0) {
      $('#deferred').toggleClass('bg-secondary bg-warning');
    }
    $('#deferred>[name=value]').text(nb_deferred);

    if (nb_blocked == 0) {
      $('#blocked').toggleClass('bg-secondary bg-danger');
    }
    $('#blocked>[name=value]').text(nb_blocked);

    $('#total>[name=value]').text(nb_sent + nb_deferred + nb_blocked);
    //} else {
    //const errorReading = settings.errorcodes[getSmtpauthTry[1]];
    //smtpErrorMessage.innerHTML = errorReading ? (errorReading + " ( Message from server : " + getSmtpauthTry[2] + " )") : "Couldn't access server.";
  }
}

function formatBytes(bytes, decimals = 2) {
  if (!+bytes) return '0 Bytes'

  const k = 1024
  const dm = decimals < 0 ? 0 : decimals
  const sizes = ['Bytes', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB']

  const i = Math.floor(Math.log(bytes) / Math.log(k))

  return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`
}